def alg(k,data_path,tempdf_hyperparamter, seed):
    
    #seed = 0

    import pandas as pd

    # df_HDtemp = pd.read_parquet(pq_path_output+'Parent_star_table.parquet')
    # df_HDtemp = df_HDtemp[['Dataset','K_Feature_Importance_sorted_60']]
    
    import importlib
    from datetime import datetime, timedelta
    import scipy.io
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.model_selection import train_test_split
    import os
    import numpy as np
    import random as rn
    #seed = 0
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    rn.seed(seed)
    import tensorflow as tf
    session_conf = tf.compat.v1.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
    from keras import backend as K
    tf.compat.v1.set_random_seed(seed)
    sess = tf.compat.v1.Session(graph=tf.compat.v1.get_default_graph(), config=session_conf)
    #K.set_session(sess)
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    from keras.models import Model
    from keras.layers import Dense, Input, Layer
    from keras import optimizers, initializers
  #  from tensorflow.keras.optimizers import optimizers, initializers
    from tensorflow.keras.optimizers import Adam
    #from keras import backend as K
    from tensorflow.keras import backend as K
    import sys
    #sys.path.append(path)
    import Functions as F
    importlib.reload(F)
    import Functions as F
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LinearRegression
    
    start_time = datetime.now()
   
    
    #Loading the data:
    Data = scipy.io.loadmat(data_path)
    data_arr = Data['X']
    if min(Data['Y'][:, 0])==0 :
        label_arr = Data['Y'][:, 0] 
    else :
        label_arr = Data['Y'][:, 0] - 1

    
    
    Data = MinMaxScaler(feature_range=(0, 1)).fit_transform(data_arr)
    #C_train_x, C_test_x, C_train_y, C_test_y = train_test_split(Data, label_arr, stratify=label_arr, test_size=0.2,
     #                                                           random_state=seed)
    
    #x_train, x_validate, y_train, y_validate = train_test_split(C_train_x, C_train_y, stratify=C_train_y, test_size=0.2,
     #                                                           random_state=seed)
    
            # IN case the number of sample not allow split of 20%/5CV
    num_classes = len(set(label_arr))
    total_samples = len(label_arr)
    min_test_size = num_classes / total_samples
    
    algo_reslut='Y'
    
    # Use try-except block to handle the ValueError
    try:
        if min_test_size > 0.2:
            C_train_x, C_test_x, C_train_y, C_test_y = train_test_split(Data, label_arr, stratify=label_arr, test_size=min_test_size, random_state=seed)
            x_train, x_validate, y_train, y_validate = train_test_split(C_train_x, C_train_y, stratify=C_train_y, test_size=min_test_size, random_state=seed)
        else:
            # If min_test_size is not greater than 0.2, set test_size to 0.2
            C_train_x, C_test_x, C_train_y, C_test_y = train_test_split(Data, label_arr, stratify=label_arr, test_size=0.2, random_state=seed)
            x_train, x_validate, y_train, y_validate = train_test_split(C_train_x, C_train_y, stratify=C_train_y, test_size=0.2, random_state=seed)

    except ValueError as e:
        # Handle the least populated class error
        accuracy ='error'
        formatted_running_time ='error'
        running_time_Sec ='error'
        listofk ='error'
        print("ValueError:", e)
        algo_reslut ='error' 
        return([accuracy,formatted_running_time,running_time_Sec,listofk,algo_reslut])

    
    x_test = C_test_x
    y_test = C_test_y

    key_feture_number = k

    #################################################################################
    # Model:
    np.random.seed(seed)

    class Feature_Select_Layer(Layer):

        def __init__(self, output_dim, **kwargs):
            super(Feature_Select_Layer, self).__init__(**kwargs)
            self.output_dim = output_dim

        def build(self, input_shape):
            self.kernel = self.add_weight(name='kernel',
                                          shape=(input_shape[1],),
                                          initializer=initializers.RandomUniform(minval=0.999999, maxval=0.9999999,
                                                                                 seed=seed),
                                          trainable=True)
            super(Feature_Select_Layer, self).build(input_shape)

        def call(self, x, selection=False, k=key_feture_number):
            kernel = K.pow(self.kernel, 2)
            if selection:
                kernel_ = K.transpose(kernel)
                kth_largest = tf.math.top_k(kernel_, k=k)[0][-1]
                kernel = tf.where(condition=K.less(kernel, kth_largest), x=K.zeros_like(kernel), y=kernel)
            return K.dot(x, tf.linalg.tensor_diag(kernel))

        def compute_output_shape(self, input_shape):
            return (input_shape[0], self.output_dim)

    # --------------------------------------------------------------------------------------------------------------------------------
    def Autoencoder(p_data_feature=x_train.shape[1], \
                    p_encoding_dim=key_feture_number, \
                    p_learning_rate=1E-3):
        input_img = Input(shape=(p_data_feature,), name='input_img')

        encoded = Dense(p_encoding_dim, activation='linear', kernel_initializer=initializers.glorot_uniform(seed))(
            input_img)
        bottleneck = encoded
        decoded = Dense(p_data_feature, activation='linear', kernel_initializer=initializers.glorot_uniform(seed))(
            encoded)

        latent_encoder = Model(input_img, bottleneck)
        autoencoder = Model(input_img, decoded)

        autoencoder.compile(loss='mean_squared_error', optimizer=optimizers.Adam(learning_rate=p_learning_rate))

      #  print('Autoencoder Structure-------------------------------------')
        autoencoder.summary()
        return autoencoder, latent_encoder

    # --------------------------------------------------------------------------------------------------------------------------------
    def Identity_Autoencoder(p_data_feature=x_train.shape[1], \
                             p_encoding_dim=key_feture_number, \
                             p_learning_rate=1E-3):
        input_img = Input(shape=(p_data_feature,), name='autoencoder_input')

        feature_selection = Feature_Select_Layer(output_dim=p_data_feature, \
                                                 input_shape=(p_data_feature,), \
                                                 name='feature_selection')

        feature_selection_score = feature_selection(input_img)

        encoded = Dense(p_encoding_dim, \
                        activation='linear', \
                        kernel_initializer=initializers.glorot_uniform(seed), \
                        name='autoencoder_hidden_layer')

        encoded_score = encoded(feature_selection_score)

        bottleneck_score = encoded_score

        decoded = Dense(p_data_feature, \
                        activation='linear', \
                        kernel_initializer=initializers.glorot_uniform(seed), \
                        name='autoencoder_output')

        decoded_score = decoded(bottleneck_score)

        latent_encoder_score = Model(input_img, bottleneck_score)
        autoencoder = Model(input_img, decoded_score)

        autoencoder.compile(loss='mean_squared_error', \
                            optimizer=optimizers.Adam(learning_rate=p_learning_rate))

     #   print('Autoencoder Structure-------------------------------------')
        autoencoder.summary()
        return autoencoder, latent_encoder_score

    # --------------------------------------------------------------------------------------------------------------------------------
    def Fractal_Autoencoder(p_data_feature=x_train.shape[1], \
                            p_feture_number=key_feture_number, \
                            p_encoding_dim=key_feture_number, \
                            p_learning_rate=1E-3,\
                            p_loss_weight_1=1, \
                            p_loss_weight_2=2):
        input_img = Input(shape=(p_data_feature,), name='autoencoder_input')

        feature_selection = Feature_Select_Layer(output_dim=p_data_feature, \
                                                 input_shape=(p_data_feature,), \
                                                 name='feature_selection')

        feature_selection_score = feature_selection(input_img)
        feature_selection_choose = feature_selection(input_img, selection=True, k=p_feture_number)

        encoded = Dense(p_encoding_dim, \
                        activation='linear', \
                        kernel_initializer=initializers.glorot_uniform(seed), \
                        name='autoencoder_hidden_layer')

        encoded_score = encoded(feature_selection_score)
        encoded_choose = encoded(feature_selection_choose)

        bottleneck_score = encoded_score
        bottleneck_choose = encoded_choose

        decoded = Dense(p_data_feature, \
                        activation='linear', \
                        kernel_initializer=initializers.glorot_uniform(seed), \
                        name='autoencoder_output')

        decoded_score = decoded(bottleneck_score)
        decoded_choose = decoded(bottleneck_choose)

        latent_encoder_score = Model(input_img, bottleneck_score)
        latent_encoder_choose = Model(input_img, bottleneck_choose)
        feature_selection_output = Model(input_img, feature_selection_choose)
        autoencoder = Model(input_img, [decoded_score, decoded_choose])

        autoencoder.compile(loss=['mean_squared_error', 'mean_squared_error'], \
                            loss_weights=[p_loss_weight_1, p_loss_weight_2], \
                            optimizer=Adam(learning_rate=p_learning_rate))

    #    print('Autoencoder Structure-------------------------------------')
        autoencoder.summary()
        return autoencoder, feature_selection_output, latent_encoder_score, latent_encoder_choose

    def ETree(p_train_feature, p_train_label, p_test_feature, p_test_label, p_seed):
        clf = ExtraTreesClassifier(n_estimators=50, random_state=p_seed)
        clf.fit(p_train_feature, p_train_label)
        accuracy = accuracy_score(np.array(p_test_label), clf.predict(p_test_feature))
        return accuracy


    # Running The Model:
    epochs_number = tempdf_hyperparamter['epochs_number'][0] # All scores run with 200
    batch_size_value = tempdf_hyperparamter['batch_size_value'][0] # 16

    #loss_weight_1 = tempdf_hyperparamter['loss_weight_1'][0]  # 0.0078125
    F_AE, \
    feature_selection_output, \
    latent_encoder_score_F_AE, \
    latent_encoder_choose_F_AE = Fractal_Autoencoder(p_data_feature=x_train.shape[1], \
                                                     p_feture_number=key_feture_number, \
                                                     p_learning_rate= 1E-3,\
                                                     p_loss_weight_1=1,\
                                                     p_loss_weight_2=2)

    F_AE_history = F_AE.fit(x_train, [x_train, x_train], \
                            epochs=epochs_number, \
                            batch_size=batch_size_value, \
                            shuffle=True, \
                            validation_data=(x_validate, [x_validate, x_validate]))

    p_data = F_AE.predict(x_test)
    numbers = x_test.shape[0] * x_test.shape[1]
    FS_layer_output = feature_selection_output.predict(x_test)
    key_features = F.top_k_keepWeights_1(F_AE.get_layer(index=1).get_weights()[0], key_feture_number)

    p_seed = seed
    selected_position_list = np.where(key_features > 0)[0]
    C_Selected_X_Train = C_train_x[:, selected_position_list]
    C_Selected_X_Test = C_test_x[:, selected_position_list]
    train_label = C_train_y
    test_label = C_test_y
    accuracy = ETree(C_Selected_X_Train,C_train_y,C_Selected_X_Test,C_test_y,seed)
    #accuracy = str(round(accuracy,2) * 100) +'%'
    accuracy = round(accuracy,2)
   
    listofk = selected_position_list.tolist()
    end_time = datetime.now()
    
    running_time = end_time - start_time
    
    # Calculate total seconds
    running_time_Sec = running_time.total_seconds()
    running_time_Sec= round(running_time_Sec,1)

    # Format running time as "Hour:Minutes:Seconds"
    formatted_running_time = str(running_time)
    formatted_running_time_hms = formatted_running_time.split(".")[0]
    #print("Running Time (H:M:S):", formatted_running_time_hms)
    

    return([accuracy,formatted_running_time,running_time_Sec,listofk,algo_reslut])



